import React, { Component } from 'react';
import {
  Text, View, StyleSheet, TouchableOpacity, TextInput
} from 'react-native';


export default class App extends Component {

  constructor(props){
    super(props)
    this.state = {num1:0, num2:0, res:0}
    this.calcular = this.calcular.bind(this)
  }

    // Função para retornar o calculo
    calcular(){
      let soma = parseInt(this.state.num1) + parseInt(this.state.num2)
      let r = this.state
      r.res = soma
      this.setState(r)
    }

  render() {
    return (
      <View style={styles.container}>

        {/* view especifica para acoplar os inputs */}
        <View style={styles.text}>
          <TextInput placeholder="000"
            style={styles.input}
            keyboardType="numeric"
            onChangeText={(num1)=>{this.setState({num1})}}
          />
          <TextInput placeholder="000"
            style={styles.input}
            keyboardType="numeric"
            onChangeText={(num2)=>{this.setState({num2})}}
          />
        </View>

      {/* Botão para calcular a soma */}
      <TouchableOpacity onPress={this.calcular} style={styles.btn}>
        <Text style={styles.calc}> somar </Text>
      </TouchableOpacity>

      {/* Campo de saida do resultado */}
      <Text style={styles.resultado}> {this.state.res} </Text>
      <Text style={styles.frase}>
        App de Estudos em React Native
      </Text>
      <Text style={styles.frase}>
        Erisvaldo Correia
      </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  btn: {
    backgroundColor: '#EBC79E',
  },
    input: {
    height:80,
    textAlign:"center",
    width:"50%",
    fontSize:40,
    marginTop:60,
  },
  text: {
    flexDirection: 'row',
  },
  calc: {
    alignSelf: "center",
    fontSize:30,
    padding: 20,
    color: 'red',
  },
  resultado: {
    alignSelf: "center",
    fontSize:35,
    padding:30,
  },
  frase: {
    alignSelf: "center",
  }
});
